package com.opsgenie.onboarding;

import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.repository.TeamDynamoRepositoryImpl;
import com.opsgenie.onboarding.repository.UserDynamoRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeamDynamoRepositoryImplTest {

    @InjectMocks
    private TeamDynamoRepositoryImpl teamRepository;

    @Mock
    DynamoDbConfig<Team> dynamoDbConfig;

    @Test
    public void shouldReturnUsers() throws Exception {


        final List<Team> expectedTeams = Arrays.asList(
                new Team ("1", "team1", new ArrayList<>()),
                new Team ("2", "team2", new ArrayList<>()),
                new Team ("3", "team3", new ArrayList<>())
        );

        when(dynamoDbConfig.scan(Team.class)).thenReturn(expectedTeams);
        final List<Team> teams = teamRepository.getAllTeams();

        verify(dynamoDbConfig, times(1)).scan(Team.class);
        assertThat(teams,  is(equalTo(expectedTeams)));
    }

    @Test
    public void shouldGetTeam() throws Exception{
        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        when(dynamoDbConfig.load(Team.class, "1")).thenReturn(expectedTeam);
        Team team = teamRepository.getTeam("1");
        assertThat(team,  is(equalTo(expectedTeam)));


    }

    @Test
    public void shouldAddTeam() throws Exception{
        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        teamRepository.addTeam(expectedTeam);

        verify(dynamoDbConfig, times(1)).save(expectedTeam);

    }

    @Test
    public void shouldUpdateTeam() throws Exception{

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        teamRepository.updateTeam(expectedTeam);

        verify(dynamoDbConfig, times(1)).save(expectedTeam);

    }

    @Test
    public void shouldDeleteTeam() throws Exception{
        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());
        teamRepository.deleteTeam("1");

        verify(dynamoDbConfig, times(1)).delete(Team.class,"1");

    }



}
