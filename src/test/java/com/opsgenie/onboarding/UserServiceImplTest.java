package com.opsgenie.onboarding;


import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.repository.UserRepositoryImpl;
import com.opsgenie.onboarding.service.UserServiceImpl;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;


    @Mock
    private UserRepositoryImpl userRepositoryImpl;

    /*@Before
    public void setUp(){
        userService = new UserServiceImpl(userRepositoryImpl);
    }*/

    @After
    public void tearDown() {
        userRepositoryImpl = null;
        userService = null;
    }

    @Test
    public void shouldListUsers() throws Exception {
        final List<User> expectedUsers = Arrays.asList(
                new User("1","AA"),
                new User("2","AAA"),
                new User("3","AAAA")
        );

        when(userRepositoryImpl.getAllUsers()).thenReturn(expectedUsers);
        final List<User> users = userService.getAllUsers();
        assertThat(users,  is(equalTo(expectedUsers)));
    }

    @Test
    public void shouldGetUser() throws Exception{
        User expectedUser = new User("10","new");

        when(userRepositoryImpl.getUser("10")).thenReturn(expectedUser);
        User user = userService.getUser("10");
        assertThat(user,  is(equalTo(expectedUser)));


    }

    @Test
    public void shouldAddUser() throws Exception{
        User expectedUser = new User("10","new");

        when(userRepositoryImpl.addUser(expectedUser)).thenReturn(expectedUser);
        User user = userService.addUser(expectedUser);
        assertThat(user,  is(equalTo(expectedUser)));


    }

    @Test
    public void shouldDeleteUser() throws Exception{
        User expectedUser = new User("10","new");

        userService.deleteUser("10");
        verify(userRepositoryImpl, times(1)).deleteUser("10");


    }

    @Test
    public void shouldUpdateUser() throws Exception{

        User expectedUser = new User("10","new");

        userService.updateUser(expectedUser);
        verify(userRepositoryImpl, times(1)).updateUser(expectedUser);

    }






}
