package com.opsgenie.onboarding;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.onboarding.controller.UserRestController;
import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.util.Arrays;
import java.util.List;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRestController.class)

public class UserRestControllerTest {


    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private UserService userService;



    @Test
    public void shouldReturnUsers() throws Exception {

        final List<User> expectedUsers = Arrays.asList(
                new User("1","AA"),
                new User("2","AAA"),
                new User("3","AAAA")
        );


        when(userService.getAllUsers()).thenReturn(expectedUsers);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedUsers);


        mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));



        verify(userService, times(1)).getAllUsers();
    }

    @Test
    public void shouldGetUser() throws Exception{
        User expectedUser = new User("1","AA");


        when(userService.getUser("1")).thenReturn( expectedUser);


        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedUser);


        mockMvc.perform(get("/users/1"))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(content().json(jsonOutput));

        verify(userService, times(1)).getUser("1");

    }


    @Test
    public void shouldAddUser() throws Exception {
        User expectedUser = new User("10","new");

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedUser);

        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonOutput))
                .andExpect(status().isOk());
    }


    @Test
    public void shouldUpdateUser() throws Exception {

        User expectedUser = new User("1", "new");
        String id = expectedUser.getId();

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedUser);


        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.put("/users/" + id).
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(jsonOutput);


        this.mockMvc.perform(builder)
                .andExpect(status()
                        .isOk())
                .andDo(print());

    }

    @Test
    public void shouldDeleteUser() throws Exception{

        User expectedUser = new User("1","AA");

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedUser);

        mockMvc.perform(delete("/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonOutput))
                .andExpect(status().isOk());
    }
}
