package com.opsgenie.onboarding;


import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.repository.UserDynamoRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDynamoRepositoryImplTest {

    @InjectMocks
    private UserDynamoRepositoryImpl userRepository;

    @Mock
    DynamoDbConfig<User> dynamoDbConfig;

    /*@Before
    public void init() {
        userRepository = new UserDynamoRepositoryImpl();
    }*/

    @Test
    public void shouldReturnUsers() throws Exception {

        final List<User> expectedUsers = Arrays.asList(
                new User("1","AA"),
                new User("2","AAA"),
                new User("3","AAAA")
        );


        when(dynamoDbConfig.scan(User.class)).thenReturn(expectedUsers);
        final List<User> users = userRepository.getAllUsers();

        verify(dynamoDbConfig, times(1)).scan(User.class);
        assertThat(users,  is(equalTo(expectedUsers)));
    }


    @Test
    public void shouldGetUser() throws Exception{
        User expectedUser = new User("10","new");

        when(dynamoDbConfig.load(User.class, "10")).thenReturn(expectedUser);
        User user = userRepository.getUser("10");
        assertThat(user,  is(equalTo(expectedUser)));


    }

    @Test
    public void shouldAddUser() throws Exception{
        User expectedUser = new User("10","new");

        when(dynamoDbConfig.save(expectedUser)).thenReturn(expectedUser);
        User user = userRepository.addUser(expectedUser);
        assertThat(user,  is(equalTo(expectedUser)));


    }

    @Test
    public void shouldUpdateUser() throws Exception{

        User expectedUser = new User("10","new");

        when(dynamoDbConfig.save(expectedUser)).thenReturn(expectedUser);
        User user = userRepository.updateUser(expectedUser);
        assertThat(user,  is(equalTo(expectedUser)));

    }

    @Test
    public void shouldDeleteUser() throws Exception{
        //User expectedUser = new User("10","new");

        userRepository.deleteUser( "10");

        verify(dynamoDbConfig, times(1)).delete(User.class,"10");

    }




}
