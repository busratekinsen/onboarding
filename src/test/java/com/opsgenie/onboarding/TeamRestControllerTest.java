package com.opsgenie.onboarding;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.onboarding.controller.TeamRestController;
import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.service.TeamService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collections;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamRestController.class)
public class TeamRestControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private TeamService teamService;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        when(teamService.getAllTeams()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));

        verify(teamService, times(1)).getAllTeams();
    }

    @Test
    public void shouldGetTeam() throws Exception{
        //User expectedUser = new User("1","AA");
        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        when(teamService.getTeam("1")).thenReturn(expectedTeam);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);


        mockMvc.perform(get("/teams/1"))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(content().json(jsonOutput));

        verify(teamService, times(1)).getTeam("1");

    }

    @Test
    public void shouldAddTeam() throws Exception {
        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);

        mockMvc.perform(post("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonOutput))
                .andExpect(status().isOk());
    }


    @Test
    public void shouldUpdateTeam() throws Exception {

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());
        String id = expectedTeam.getTeamId();

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);


        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.put("/teams/" + id).
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(jsonOutput);


        this.mockMvc.perform(builder)
                .andExpect(status()
                        .isOk())
                .andDo(print());

    }


    @Test
    public void shouldDeleteTeam() throws Exception{

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);

        mockMvc.perform(delete("/teams/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonOutput))
                .andExpect(status().isOk());
    }



}
