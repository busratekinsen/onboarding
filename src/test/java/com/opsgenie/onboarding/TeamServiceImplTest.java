package com.opsgenie.onboarding;

import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.repository.TeamRepository;
import com.opsgenie.onboarding.repository.TeamRepositoryImpl;
import com.opsgenie.onboarding.service.TeamService;
import com.opsgenie.onboarding.service.TeamServiceImpl;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {


    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepositoryImpl teamRepositoryImpl;

    /*@Before
    public void setUp() {
        teamService = new TeamServiceImpl(teamRepositoryImpl);
    }*/

    @After
    public void tearDown() {
        teamRepositoryImpl = null;
        teamService = null;
    }


    @Test
    public void shouldListTeams() throws Exception {

        teamService.getAllTeams();
        verify(teamRepositoryImpl, times(1)).getAllTeams();
    }

    @Test
    public void shouldGetTeam() throws Exception {

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        when(teamRepositoryImpl.getTeam("1")).thenReturn(expectedTeam);
        Team team = teamService.getTeam("1");
        assertThat(team,  is(equalTo(expectedTeam)));
    }


    @Test
    public void shouldAddTeam() throws Exception {

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        teamService.addTeam(expectedTeam);
        verify(teamRepositoryImpl, times(1)).addTeam(expectedTeam);

    }


    @Test
    public void shouldUpdateTeam() throws Exception {

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        teamService.updateTeam(expectedTeam);
        verify(teamRepositoryImpl, times(1)).updateTeam(expectedTeam);

    }

    @Test
    public void shouldDeleteTeam() throws Exception {

        Team expectedTeam = new Team ("1", "team1", new ArrayList<>());

        teamService.deleteTeam("1");
        verify(teamRepositoryImpl, times(1)).deleteTeam("1");

    }
}
