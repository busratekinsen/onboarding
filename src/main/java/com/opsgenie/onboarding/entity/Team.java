package com.opsgenie.onboarding.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.ArrayList;
import java.util.Objects;


@DynamoDBTable(tableName = "onboarding_team")
public class Team {

    private String teamId;
    private String teamName;
    private ArrayList<String> userIds;


    public Team(){

    }

    public Team(String teamId, String teamName, ArrayList<String> userIds) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.userIds = userIds;
    }

    @DynamoDBHashKey(attributeName = "teamId")
    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    @DynamoDBAttribute(attributeName = "teamName")
    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @DynamoDBAttribute(attributeName = "userIds")
    public ArrayList<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(ArrayList<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(teamId, team.teamId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(teamId);
    }

}
