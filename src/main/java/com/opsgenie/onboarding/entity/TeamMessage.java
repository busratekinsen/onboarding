package com.opsgenie.onboarding.entity;

public class TeamMessage {
    private String teamId;
    private String userId;

    public TeamMessage() {
    }

    public TeamMessage(String teamId, String userId) {
        this.teamId = teamId;
        this.userId = userId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
