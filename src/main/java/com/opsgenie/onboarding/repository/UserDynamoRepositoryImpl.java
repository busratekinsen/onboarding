package com.opsgenie.onboarding.repository;


import com.opsgenie.onboarding.DynamoDbConfig;
import com.opsgenie.onboarding.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository(value="DynamoDbImpl")
public class UserDynamoRepositoryImpl implements UserRepository {

    @Autowired
    private DynamoDbConfig<User> dynamoDbConfig;





    @Override
    public List<User> getAllUsers() {
        return dynamoDbConfig.scan(User.class);

    }

    @Override
    public User getUser(String id) {
        return  dynamoDbConfig.load(User.class, id);
    }

    @Override
    public User addUser(User user) {

        dynamoDbConfig.save(user);
        return user;

    }

    @Override
    public void deleteUser(String id) {
        dynamoDbConfig.delete(User.class, id);


    }

    @Override
    public User updateUser(User user) {

        dynamoDbConfig.save(user);
         return user;
    }
}
