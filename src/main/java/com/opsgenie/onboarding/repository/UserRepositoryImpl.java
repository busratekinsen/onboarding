package com.opsgenie.onboarding.repository;

import com.opsgenie.onboarding.entity.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository(value="inMemoryImpl")
public class UserRepositoryImpl implements UserRepository {

    ArrayList<User> userList;

    public UserRepositoryImpl() {
        this.userList = new ArrayList<>();
        User user1= new User("1", "AA");
        User user2= new User("2", "AAA");
        User user3= new User("3", "AAAA");

        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
    }

    @Override
    public List<User> getAllUsers() {
        return userList;
    }

    @Override
    public User getUser(String id) {
        User user=null;
        for(int i=0;i<userList.size();i++){
            if(userList.get(i).getId().equals(id)){
                user = userList.get(i);

            }
        }
        return user;

    }
    @Override
    public User addUser( User user) {

        userList.add(user);
        return user;

    }

    @Override
    public void deleteUser(String id) {
        User user=null;
        for(int i=0;i<userList.size();i++){
            if(userList.get(i).getId().equals(id)){
                user=userList.get(i);
                //userList.remove(userList.get(i));
            }
        }

        if(user!=null){
            userList.remove(user);
        }


    }

    @Override
    public User updateUser(User user) {
        String id=user.getId();
        Boolean control=true;



        for(int i=0;i<userList.size();i++){
            if(userList.get(i).getId().equals(id)){
                userList.remove(userList.get(i));
                user.setId(id);
                userList.add(user);

            }
        }
        return user;
    }
}
