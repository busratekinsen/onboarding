package com.opsgenie.onboarding.repository;

import com.opsgenie.onboarding.entity.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface UserRepository {

    List<User> getAllUsers();
    User getUser(String id);

    User addUser(User user);
    void deleteUser(String id);
    User updateUser(User user);

}
