package com.opsgenie.onboarding.repository;


import com.opsgenie.onboarding.DynamoDbConfig;
import com.opsgenie.onboarding.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository( value="TeamDynamoDbImpl")
public class TeamDynamoRepositoryImpl implements TeamRepository{



    @Autowired
    private DynamoDbConfig<Team> dynamoDbConfig;


    @Override
    public List<Team> getAllTeams() {
        return dynamoDbConfig.scan(Team.class);
    }

    @Override
    public Team getTeam(String id) {
        return  dynamoDbConfig.load(Team.class, id);
    }

    @Override
    public void addTeam(Team team) {
        team.getUserIds().toString();
        dynamoDbConfig.save(team);
        //mapper.save(team);

    }

    @Override
    public void deleteTeam(String id) {

        dynamoDbConfig.delete(Team.class, id);

    }

    @Override
    public void updateTeam(Team team) {

        dynamoDbConfig.save(team);


    }

    public void addUserToTeam(String teamId, String userId){
        Team team= dynamoDbConfig.load(Team.class, teamId);

        if(team == null){
            return;
        }

        team.getUserIds().add(userId);

        dynamoDbConfig.save(team);

    }
}
