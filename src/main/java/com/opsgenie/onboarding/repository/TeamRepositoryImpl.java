package com.opsgenie.onboarding.repository;

import com.opsgenie.onboarding.entity.Team;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository(value="teaminMemoryImpl")
public class TeamRepositoryImpl implements TeamRepository {

    ArrayList<Team> teamList;

    public TeamRepositoryImpl() {
        this.teamList = new ArrayList<>();
    }

    @Override
    public List<Team> getAllTeams() {
        return teamList;
    }

    @Override
    public Team getTeam(String id) {
        Team team=null;
        for(int i=0;i<teamList.size();i++){
            if(teamList.get(i).getTeamId().equals(id)){
                team = teamList.get(i);

            }
        }
        return team;
    }

    @Override
    public void addTeam(Team team) {


        //String newId=UUID.randomUUID().toString();
        //team.setTeamId(newId);
        teamList.add(team);
    }

    @Override
    public void deleteTeam(String id) {
        Team team=null;

        for(int i=0;i<teamList.size();i++){
            if(teamList.get(i).getTeamId().equals(id)){
                team=teamList.get(i);

            }
        }

        if(team!=null){
            teamList.remove(team);
        }

    }

    @Override
    public void updateTeam(Team team) {
        String id=team.getTeamId();

        for(int i=0;i<teamList.size();i++){
            if(teamList.get(i).getTeamId().equals(id)){
                teamList.remove(teamList.get(i));
                team.setTeamId(id);
                teamList.add(team);

            }
        }

    }

    @Override
    public void addUserToTeam(String teamId, String userId) {

    }
}
