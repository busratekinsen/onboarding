package com.opsgenie.onboarding.repository;

import com.opsgenie.onboarding.entity.Team;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TeamRepository {
    List<Team> getAllTeams();
    Team getTeam(String id);

    void addTeam(Team team);
    void deleteTeam(String id);
    void updateTeam(Team team);
    void addUserToTeam(String teamId, String userId);

}
