package com.opsgenie.onboarding;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.*;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.entity.TeamMessage;
import com.opsgenie.onboarding.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Component

public class QueueProcessor {

    private AmazonSQS sqs;
    @Value("${aws.client.accessKey}")
    private String accessKey;
    @Value("${aws.client.secretKey}")
    private String secretKey;

    AWSCredentials awsCredentials;
    AWSCredentialsProvider awsCredentialsProvider;

    @Autowired
    private TeamService teamService;



    @PostConstruct
    public void init(){
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        this.sqs=AmazonSQSClientBuilder.standard().withRegion("us-west-2").withCredentials(awsCredentialsProvider).build();
    }


    public void sendMessageToQueue(String message){
        String queueUrl=this.sqs.getQueueUrl("team_assign_team_member_queue").getQueueUrl();
        SendMessageResult messageResult =  this.sqs.sendMessage(new SendMessageRequest(queueUrl, message));
        System.out.println(messageResult.toString());
    }

    @Scheduled(fixedRate = 1000)
    public void receiveMessageFromQueue() throws Exception{//poll

        String queueUrl=this.sqs.getQueueUrl("team_assign_team_member_queue").getQueueUrl();
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        if(messages.size()==0){
            return ;
        }
        String message=messages.get(0).getBody();

        //JSON from String to Object
        ObjectMapper mapper = new ObjectMapper();


        TeamMessage teamMessage = mapper.readValue(message, TeamMessage.class);

        System.out.println(message);


        teamService.receiveTeamMemberFromQueue(teamMessage);

        this.sqs.deleteMessage(queueUrl,messages.get(0).getReceiptHandle() );

    }



    public AmazonSQS getSqs() {
        return sqs;
    }

    public void setSqs(AmazonSQS sqs) {
        this.sqs = sqs;
    }



}
