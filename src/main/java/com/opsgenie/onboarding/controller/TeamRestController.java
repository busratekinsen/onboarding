package com.opsgenie.onboarding.controller;


import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamRestController {

    @Autowired
   private TeamService teamService;


    @RequestMapping( method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Team>> getTeams(){

        return new ResponseEntity(teamService.getAllTeams(),HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> getTeam(@PathVariable("id") String id){

        Team team= teamService.getTeam(id);

        return new ResponseEntity(team,HttpStatus.OK);

    }


    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity<ArrayList<Team>> addTeam(@RequestBody Team newTeam){

        teamService.addTeam(newTeam);

        return new ResponseEntity(HttpStatus.OK);


    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTeam(@PathVariable("id") String id){

        teamService.deleteTeam(id);

        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateTeam(@PathVariable("id") String id, @RequestBody Team updatedTeam){
        updatedTeam.setTeamId(id);
        teamService.updateTeam(updatedTeam);

        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}/assignTeamMember", method = RequestMethod.POST)
    public ResponseEntity assignTeamMember(@PathVariable("id") String id, @RequestBody List<String> userIds){

        teamService.assignTeamMemberToQueue(id,userIds);
        //return new ResponseEntity.status(HttpStatus.ACCEPTED);
        return new ResponseEntity(HttpStatus.ACCEPTED);

    }


}
