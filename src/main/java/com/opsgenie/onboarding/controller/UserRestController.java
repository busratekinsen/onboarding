package com.opsgenie.onboarding.controller;


import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.service.UserService;
import com.opsgenie.onboarding.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserRestController {

    @Autowired
    private UserService userService;



    @RequestMapping( method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers(){

        return new ResponseEntity(userService.getAllUsers(),HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> getUser(@PathVariable("id") String id){

        User user= userService.getUser(id);

        return new ResponseEntity(user,HttpStatus.OK);

    }


    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity<Void> addUser(@RequestBody User newUser){

        userService.addUser(newUser);

        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable("id") String id){

        userService.deleteUser(id);

        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateUser(@PathVariable("id") String id, @RequestBody User updatedUser){
        updatedUser.setId(id);
        userService.updateUser(updatedUser);

        return new ResponseEntity(HttpStatus.OK);

    }


}
