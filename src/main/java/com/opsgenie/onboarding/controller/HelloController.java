package com.opsgenie.onboarding.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @RequestMapping("/asd")
    public ResponseEntity hello(){
        return ResponseEntity.status(HttpStatus.OK).body("hello");
    }
}
