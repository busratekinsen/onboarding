package com.opsgenie.onboarding.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.onboarding.QueueProcessor;
import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.entity.TeamMessage;
import com.opsgenie.onboarding.repository.TeamRepository;
import com.opsgenie.onboarding.repository.TeamRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sun.plugin2.message.Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    @Qualifier("TeamDynamoDbImpl")
    private TeamRepository teamRepositoryIml;

    @Autowired
    private QueueProcessor queueProcessor;


    @Override
    public List<Team> getAllTeams() {
        return teamRepositoryIml.getAllTeams();
    }

    @Override
    public Team getTeam(String id) {
        return teamRepositoryIml.getTeam(id);
    }

    @Override
    public void addTeam(Team team) {


        String newId=UUID.randomUUID().toString();
        team.setTeamId(newId);
        teamRepositoryIml.addTeam(team);

    }

    @Override
    public void deleteTeam(String id) {
        teamRepositoryIml.deleteTeam(id);

    }

    @Override
    public void updateTeam(Team team) {
        teamRepositoryIml.updateTeam(team);



    }



    @Override
    public void assignTeamMemberToQueue(String id, List<String> userIds)  {
        for(String userId:userIds){
            TeamMessage message = new TeamMessage(id,userId);

            ObjectMapper mapper = new ObjectMapper();
            String jsonOutput = null;
            try {
                jsonOutput = mapper.writeValueAsString(message);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            //queueProcessor.getSqs();

            queueProcessor.sendMessageToQueue(jsonOutput);

        }






    }


    @Override
    public void receiveTeamMemberFromQueue(TeamMessage message) {

        //call repository
        teamRepositoryIml.addUserToTeam(message.getTeamId(),message.getUserId());




    }



}
