package com.opsgenie.onboarding.service;

import com.opsgenie.onboarding.entity.Team;
import com.opsgenie.onboarding.entity.TeamMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public interface TeamService {

    List<Team> getAllTeams();

    Team getTeam(String id);

    void addTeam(Team team);

    void deleteTeam(String id);

    void updateTeam(Team team);

    void assignTeamMemberToQueue(String id,List<String> userIds);

    void receiveTeamMemberFromQueue(TeamMessage teamMessage);
}
