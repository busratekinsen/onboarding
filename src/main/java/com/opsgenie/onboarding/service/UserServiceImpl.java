package com.opsgenie.onboarding.service;


import com.opsgenie.onboarding.entity.User;
import com.opsgenie.onboarding.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    @Qualifier("DynamoDbImpl")
    private  UserRepository userRepository ;


    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public User getUser(String id) {
        return userRepository.getUser(id);
    }

    @Override
    public User addUser(User user) {

        String newId = UUID.randomUUID().toString();
        user.setId(newId);
        userRepository.addUser(user);
        return user;
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteUser(id);
    }

    @Override
    public User updateUser(User user) {
        userRepository.updateUser(user);
        return user;
    }


}
