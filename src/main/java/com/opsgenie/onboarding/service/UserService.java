package com.opsgenie.onboarding.service;

import com.opsgenie.onboarding.entity.User;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public interface UserService {


    List<User> getAllUsers();

    User getUser(String id);

    User addUser(User user);

    void deleteUser(String id);

    User updateUser(User user);

}
