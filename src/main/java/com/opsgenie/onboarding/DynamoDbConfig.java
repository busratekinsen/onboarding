package com.opsgenie.onboarding;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

@Component
public class DynamoDbConfig<T> {

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;


    public DynamoDbConfig() {
        AWSCredentials awsCredentials = new BasicAWSCredentials("fake", "fake");
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("http://localhost:3966", "us-west-2"))
                .withCredentials(awsCredentialsProvider).build();


        mapper= new DynamoDBMapper(client);

    }



    public T save(T t) {
        mapper.save(t);
        return t;
    }

    public T load(Class className, String id){
        return (T)mapper.load(className,id);
    }

    public List<T> scan(Class className){
        List<T> ourList = new ArrayList<>();
        mapper.scan(className,new DynamoDBScanExpression()).forEach(var -> ourList.add((T)var));
        return ourList;
    }

    public void delete(Class className, String id){
        mapper.delete(mapper.load(className, id));
    }



    public AmazonDynamoDB getClient() {
        return client;
    }

    public void setClient(AmazonDynamoDB client) {
        this.client = client;
    }

    public DynamoDBMapper getMapper() {
        return mapper;
    }

    public void setMapper(DynamoDBMapper mapper) {
        this.mapper = mapper;
    }




}
